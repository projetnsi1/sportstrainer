let start_time;

function commencerJeu() {
    document.getElementById("boutonCommencer").style.display = "none";
    document.getElementById("label").innerText = "Attendez...";
    let tempsAttente = Math.random() * (5 - 1) + 1;
    setTimeout(afficherFeuDepart, tempsAttente * 1000);
}

function afficherFeuDepart() {
    let canvasFeu = document.getElementById("canvasFeu").getContext("2d");

    canvasFeu.beginPath();
    canvasFeu.arc(50, 30, 10, 0, Math.PI * 2);
    canvasFeu.fillStyle = "red";
    canvasFeu.fill();
    canvasFeu.closePath();
    setTimeout(() => {
        canvasFeu.clearRect(0, 0, 100, 150);
        canvasFeu.beginPath();
        canvasFeu.arc(50, 50, 10, 0, Math.PI * 2);
        canvasFeu.fillStyle = "yellow";
        canvasFeu.fill();
        canvasFeu.closePath();
        setTimeout(() => {
            canvasFeu.clearRect(0, 0, 100, 150);
            canvasFeu.beginPath();
            canvasFeu.arc(50, 70, 10, 0, Math.PI * 2);
            canvasFeu.fillStyle = "green";
            canvasFeu.fill();
            canvasFeu.closePath();

            start_time = new Date().getTime();
            document.getElementById("boutonJouer").disabled = false;
            document.getElementById("boutonJouer").style.backgroundColor = "#4CAF50";
            document.getElementById("boutonJouer").style.color = "white";
            document.getElementById("boutonJouer").style.border = "2px solid";
            document.getElementById("boutonJouer").style.fontFamily = "Arial";
            document.getElementById("boutonJouer").style.fontSize = "12px";
            document.getElementById("boutonJouer").style.padding = "10px";
            document.getElementById("boutonJouer").style.marginTop = "20px";
            document.getElementById("boutonJouer").innerText = "Jouer!";
        }, Math.random() * (1500 - 500) + 500);
    }, Math.random() * (1500 - 500) + 500);
}

function calculerTempsReaction() {
    if (!start_time) {
        return;
    }

    let end_time = new Date().getTime();
    let reaction_time = (end_time - start_time) / 1000;

    document.getElementById("label").innerText = `Votre temps de réaction est de ${reaction_time.toFixed(3)} secondes.`;

    let message = reaction_time < 0.2 ? "Félicitations ! Vous êtes rapide." :
        reaction_time > 0.4 ? "Tu es nul nul nul!" :
        reaction_time >= 0.15 && reaction_time <= 0.25 ? "Tu es plutôt rapide !" :
        "Tu peux mieux faire !";

    let couleur = reaction_time < 0.2 ? "#00A000" :
        reaction_time > 0.4 ? "#FF0000" :
        reaction_time >= 0.15 && reaction_time <= 0.25 ? "#0000FF" :
        "#FF8C00";

    let labelMessage = document.createElement("label");
    labelMessage.innerHTML = message;
    labelMessage.style.fontFamily = "Arial";
    labelMessage.style.fontSize = "16px";
    labelMessage.style.fontWeight = "bold";
    labelMessage.style.color = couleur;
    labelMessage.style.border = "1px solid";
    labelMessage.style.padding = "10px";
    labelMessage.style.marginTop = "10px";
    document.getElementById("messageContainer").appendChild(labelMessage);

    document.getElementById("boutonJouer").style.display = "none";
    document.getElementById("boutonRestart").style.display = "block";
}

function recommencerJeu() {
    start_time = null;
    document.getElementById("label").innerText = "Appuyez sur le bouton lorsque vous le voyez!";
    document.getElementById("boutonRestart").style.display = "none";
    document.getElementById("boutonCommencer").style.display = "block";
    document.getElementById("boutonJouer").style.display = "block"; // Ajout de cette ligne pour afficher le bouton "Jouer"
    let canvasFeu = document.getElementById("canvasFeu").getContext("2d");
    canvasFeu.clearRect(0, 0, 100, 150);
}

window.onload = function() {
    let canvasFeu = document.getElementById("canvasFeu").getContext("2d");

    let label = document.createElement("label");
    label.innerHTML = "Appuyez sur le bouton pour commencer!";
    label.id = "label";
    label.style.fontFamily = "Arial";
    label.style.fontSize = "14px";
    label.style.fontWeight = "bold";
    label.style.marginTop = "10px";
    document.getElementById("section").appendChild(label);

    let boutonCommencer = document.createElement("button");
    boutonCommencer.innerHTML = "Commencer";
    boutonCommencer.onclick = commencerJeu;
    boutonCommencer.style.width = "150px";
    boutonCommencer.style.height = "30px";
    boutonCommencer.style.fontFamily = "Arial";
    boutonCommencer.style.fontSize = "12px";
    boutonCommencer.style.backgroundColor = "#2196F3";
    boutonCommencer.style.color = "white";
    boutonCommencer.style.border = "3px solid";
    boutonCommencer.style.marginTop = "20px";
    boutonCommencer.id = "boutonCommencer";
    document.getElementById("section").appendChild(boutonCommencer);

    let boutonJouer = document.createElement("button");
    boutonJouer.innerHTML = "Jouer!";
    boutonJouer.onclick = calculerTempsReaction;
    boutonJouer.id = "boutonJouer";
    boutonJouer.style.width = "150px";
    boutonJouer.style.height = "30px";
    boutonJouer.style.fontFamily = "Arial";
    boutonJouer.style.fontSize = "12px";
    boutonJouer.style.backgroundColor = "#2196F3";
    boutonJouer.style.color = "white";
    boutonJouer.style.border = "3px solid";
    boutonJouer.style.marginTop = "20px";
    boutonJouer.disabled = true;
    document.getElementById("section").appendChild(boutonJouer);

    let boutonRestart = document.createElement("button");
    boutonRestart.innerHTML = "Restart";
    boutonRestart.onclick = recommencerJeu;
    boutonRestart.id = "boutonRestart";
    boutonRestart.style.width = "150px";
    boutonRestart.style.height = "30px";
    boutonRestart.style.fontFamily = "Arial";
    boutonRestart.style.fontSize = "12px";
    boutonRestart.style.backgroundColor = "#2196F3";
    boutonRestart.style.color = "white";
    boutonRestart.style.border = "3px solid";
    boutonRestart.style.display = "none";
    boutonRestart.style.marginTop = "20px";
    document.getElementById("section").appendChild(boutonRestart);

    let messageContainer = document.createElement("div");
    messageContainer.id = "messageContainer";
    document.getElementById("section").appendChild(messageContainer);
};


