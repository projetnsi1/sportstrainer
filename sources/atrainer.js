// Paramètres du jeu
let width = 600; // Largeur du cadre de jeu
let height = 400; // Hauteur du cadre de jeu
let targetRadius = 30;
let maxClicks = 2;
let clicksLeft = 2;
let score = 0;
let gameStarted = false;
let timer;
let currentTarget = null;
let missedClicks = 0;

// Fonction pour créer une nouvelle cible
function createTarget() {
    if (!gameStarted) return;
    if (currentTarget) return;

    const x = Math.floor(Math.random() * (width - 2 * targetRadius));
    const y = Math.floor(Math.random() * (height - 2 * targetRadius));
    const target = document.createElement('div');
    target.className = 'target';
    target.style.position = 'absolute';
    target.style.width = target.style.height = `${targetRadius * 2}px`;
    target.style.backgroundColor = '#B81414';
    target.style.borderRadius = '50%';
    target.style.left = `${x}px`;
    target.style.top = `${y}px`;

    document.getElementById('frame_game').appendChild(target);
    target.addEventListener('click', handleClick);
    currentTarget = target;
}

// Fonction pour commencer une nouvelle partie
function startGame() {
    clicksLeft = maxClicks;
    score = 0;
    missedClicks = 0;
    gameStarted = true;
    updateUI();

    clearCanvas();
    timer = setInterval(createTarget, 1000); // Apparition d'une nouvelle cible toutes les secondes
    setTimeout(gameOver, 30000); // Fin de partie après 30 secondes
    startGame();
}

// Fonction pour redémarrer une partie
function restartGame() {
    clearInterval(timer);
    document.getElementById('frameMessage').style.display = 'none';
    currentTarget = null;
    startGame();
}

// Fonction pour supprimer toutes les cibles
function clearCanvas() {
    document.querySelectorAll('.target').forEach(target => target.remove());
}

// Gestionnaire de clics sur la cible
function handleClick(event) {
    if (!gameStarted) return;

    const clickedTarget = event.target;
    if (clickedTarget === currentTarget) {
        score++;
        clickedTarget.remove();
        currentTarget = null;
        updateUI();
    } else {
        missedClicks++;
        if (missedClicks >= maxClicks) {
            gameOver();
        }
    }
}

// Mettre à jour l'interface utilisateur
function updateUI() {
    document.getElementById('labelScore').textContent = `Score : ${score}`;
}

// Fonction appelée en fin de partie
function gameOver() {
    gameStarted = false;
    clearInterval(timer);
    document.getElementById('frameMessage').style.display = 'block';
    document.getElementById('labelMessage').textContent = `Partie terminée ! Votre score final est ${score}`;
    updateUI();
}

// Fonction pour définir la difficulté
function setDifficulty(difficulty) {
    switch (difficulty) {
        case 'easy':
            targetRadius = 30;
            break;
        case 'medium':
            targetRadius = 20;
            break;
        case 'hard':
            targetRadius = 10;
            break;
        default:
            targetRadius = 30;
            break;
    }
    startGame();
}

// Gestion des événements
document.getElementById('restartButton').addEventListener('click', restartGame);
document.querySelectorAll('.difficultyButton').forEach(button => {
    button.addEventListener('click', () => setDifficulty(button.value));
});

// Appel de la fonction startGame au chargement de la page
window.onload = startGame;
















