// Variables globales pour stocker l'état du programme
let clickCount = 0;
let startTime = null;
let duration = 5;  // Durée par défaut

// Fonction appelée lors du clic sur le bouton
function onClick() {
    // Si c'est le premier clic, enregistre le temps de début et planifie la fin du test
    if (startTime === null) {
        startTime = new Date().getTime();
        setTimeout(showResults, duration * 1000);
    }
    clickCount++;
    updateLabel();
}

// Fonction pour mettre à jour l'affichage du nombre de clics par seconde
function updateLabel() {
    let elapsed = (new Date().getTime() - startTime) / 1000;
    let cps = clickCount / elapsed;
    // Arrondir à 0,25
    let roundedCps = Math.round(cps * 4) / 4;
    document.getElementById("label").textContent = `Clics par Seconde: ${roundedCps.toFixed(2)}\nDurée du test: ${duration} seconde(s)`;
}

// Fonction pour changer la durée du test
function changeDuration(newDuration) {
    duration = newDuration;
    document.getElementById("label").textContent = `Clics par Seconde: 0\nDurée du test: ${duration} seconde(s)`;
}

// Fonction pour recommencer le test
function restartTest() {
    clickCount = 0;
    startTime = null;
    document.getElementById("button").disabled = false;
    document.getElementById("restartButton2").disabled = true;
    document.getElementById("label").textContent = `Clics par Seconde: 0\nDurée du test: ${duration} seconde(s)`;
}

// Fonction pour afficher les résultats à la fin du test
function showResults() {
    document.getElementById("button").disabled = true;
    document.getElementById("restartButton2").disabled = false;
    let elapsed = (new Date().getTime() - startTime) / 1000;
    let cps = clickCount / elapsed;
    // Arrondir à 0,25
    let roundedCps = Math.round(cps * 4) / 4;
    document.getElementById("label").textContent = `Test terminé. Total de clics: ${clickCount}\nClics par seconde: ${roundedCps.toFixed(2)}`;
    showMessage(roundedCps);
}

// Fonction pour afficher un message en fonction du nombre de clics par seconde
function showMessage(cps) {
    let messageLabel = document.getElementById("messageLabel");
    if (cps < 5.50) {
        messageLabel.textContent = "🐌 Tu es un escargot ! 🐌";
    } else if (cps <= 7.50) {
        messageLabel.textContent = "🦏 Tu es un rhinocéros ! 🦏";
    } else if (cps <= 9.50) {
        messageLabel.textContent = "🐅 Tu es un tigre ! 🐅";
    } else {
        messageLabel.textContent = "👑 Tu es le goat du clic ! Mais tu ne dépasseras jamais Faker ! 👑";
    }
}

document.addEventListener('DOMContentLoaded', function() {
    const root = document.getElementById('root');

    const label = document.createElement('label');
    label.id = 'label';
    label.textContent = `Clics par Seconde: 0\nDurée du test: 5 seconde(s)`;
    root.appendChild(label);

    const button = document.createElement('button');
    button.id = 'button';
    button.textContent = 'Cliquez ici';
    button.addEventListener('click', onClick);
	button.classList.add('main-button');
    root.appendChild(button);

    const restartButton = document.createElement('button');
    restartButton.id = 'restartButton2';
    restartButton.textContent = 'Recommencer';
    restartButton.addEventListener('click', restartTest);
    restartButton.disabled = true;
    root.appendChild(restartButton);

    // Ajout de la nouvelle div pour les boutons de durée
    const durationButtonsContainer = document.createElement('div');
    durationButtonsContainer.id = 'durationButtonsContainer';
    root.appendChild(durationButtonsContainer);

    // Code pour créer les boutons de durée
    const durationButtons = [
        { text: "1 Seconde", duration: 1 },
        { text: "2 Secondes", duration: 2 },
        { text: "5 Secondes", duration: 5 },
        { text: "10 Secondes", duration: 10 }
    ];

    durationButtons.forEach(item => {
        const durButton = document.createElement('button');
        durButton.textContent = item.text;
        durButton.addEventListener('click', () => changeDuration(item.duration));
        durationButtonsContainer.appendChild(durButton); // Ajoute le bouton à la nouvelle div
    });

    const messageLabel = document.createElement('label');
    messageLabel.id = 'messageLabel';
    root.appendChild(messageLabel);
});

